"use strict";
var { app, BrowserWindow, Menu, ipcMain, shell, dialog } = require("electron");
var initialised = false;

var WhaleMemo = (function () {
    function WhaleMemo(app, onInitialized) {
        this.onInitialized = onInitialized;
        this.mainWindow = null;
        this.mainURL = "file://" + __dirname + "/../html/index.html";
        this.app = app;
        this.app.on('window-all-closed', this.onWindowAllClosed.bind(this));
        this.app.on('ready', this.create.bind(this));
        this.app.on('activate', this.onActivated.bind(this));
        app.requestSingleInstanceLock()
        app.on('second-instance', (event, argv, cwd) => {
            if (this.mainWindow === null) return;
            if (this.mainWindow.isMinimized()) {
                this.mainWindow.restore();
            }
            this.mainWindow.focus();
        })
    }
    WhaleMemo.prototype.onWindowAllClosed = function () {
        this.app.quit();
    };
    WhaleMemo.prototype.create = function () {
        this.mainWindow = new BrowserWindow({
            title: "Whale Memo",
            width: 10,
            height: 10,
            acceptFirstMouse: true,
            nodeIntegration: true,
            webPreferences: {
                nodeIntegration: true
            },
            frame: false,
            titleBarStyle: 'hiddenInset',
            icon: "file://" + __dirname + "/../image/whale.png"
        });
        initMenu(this.mainWindow);
        openLinkInBrowser(this.mainWindow);
        this.mainWindow.on('closed', () => {
            this.mainWindow = null;
        });
        var onFinalClosing = false;
        this.mainWindow.on('close', (event) => {
            if (!onFinalClosing) {
                event.sender.send("onWindowClosing");
                event.preventDefault();
                return;
            }
        });

        this.mainWindow.on('blur', (event) => {
            event.sender.send("onBlur");
        });
        ipcMain.on("closeCommandAfterSynce", () => {
            onFinalClosing = true;
            app.quit();
        });
        ipcMain.on("initialized", () => {
            if (!initialised) {
                initialised = true;
                const screenSize = require("electron").screen.getPrimaryDisplay().workAreaSize;
                this.mainWindow.setBounds({ x: Math.floor((screenSize.width - 800) / 2), y: Math.floor((screenSize.height - 400) / 2), width: 800, height: 400 });
                if (this.onInitialized) {
                    this.onInitialized();
                }
            }
        });
        if (process.env.DEBUG) {
            this.mainWindow.webContents.openDevTools();
        }
        this.mainWindow.loadURL(this.mainURL);
    };
    WhaleMemo.prototype.onReady = function () {
        this.create();
    };
    WhaleMemo.prototype.onActivated = function () {
        if (this.mainWindow === null) {
            this.create();
        }
    };
    var subWindows = [];
    ipcMain.on("openNewWindow", (event, note) => {
        if (subWindows[note.id]) {
            subWindows[note.id].focus();
            return;
        }
        var oneNoteWindow = new BrowserWindow({
            width: 1600,
            height: 600,
            nodeIntegration: true,
            webPreferences: {
                nodeIntegration: true
            },
            frame: false,
            titleBarStyle: 'hiddenInset'
        });
        subWindows[note.id] = oneNoteWindow;
        oneNoteWindow.on('closed', () => {
            delete subWindows[note.id];
            event.sender.send("windowClosed", note);
        });

        initMenu(oneNoteWindow);
        openLinkInBrowser(oneNoteWindow);
        oneNoteWindow.note = note;
        oneNoteWindow.loadURL('file://' + __dirname + '/../html/oneNoteIndex.html', { note: note });

        if (process.env.DEBUG) {
            oneNoteWindow.webContents.openDevTools();
        }
    })

    ipcMain.on("updateNote", (event, note) => {
        subWindows[note.id].webContents.send('updateNote', note);
    })
    return WhaleMemo;
}());
function openLinkInBrowser(mainWindow) {
    mainWindow.webContents.on('new-window', function (e, url) {
        e.preventDefault();
        shell.openExternal(url);
    });
}
function initMenu(mainWindow) {
    const templateMenu = [
        {
            label: 'about',
            submenu: [
                {
                    role: 'appMenu',
                }
            ]
        },
        {
            label: 'File',
            submenu: [
                {
                    label: 'Save',
                    accelerator: 'CmdOrCtrl+S',
                    click() {
                        mainWindow.webContents.send('save');
                    },
                },
                {
                    label: 'New',
                    accelerator: 'CmdOrCtrl+N',
                    click() {
                        mainWindow.webContents.send('append');
                    },
                },
                /*...((process.platform == "darwin") ? [{
                    label: 'Speech',
                    submenu: [
                        { role: 'startspeaking' },
                        { role: 'stopspeaking' }
                    ]
                }] : []),*/
                {
                    label: 'Import',
                    submenu: [
                        {
                            label: 'from Evernote(.enex)',
                            click: () => {
                                dialog.showOpenDialog({ properties: ['openFile'] }, (filePath) => {
                                    mainWindow.webContents.send('importFile', filePath);
                                })
                            }
                        }
                    ]
                }
            ]
        },
        {
            label: "Edit",
            submenu: [
                { role: "undo" },
                { role: "redo" },
                { role: "cut" },
                { role: "copy" },
                { role: "paste" },
                { role: "selectall" },
                {
                    type: 'separator',
                },
                {
                    role: 'togglefullscreen',
                }
            ]
        }
    ];
    if (process.env.DEBUG) {
        templateMenu.filter(m => m.label == "Edit")[0].submenu.push({ role: "reload" })
        templateMenu.filter(m => m.label == "Edit")[0].submenu.push({
            label: 'Open devtool',
            accelerator: 'CmdOrCtrl+D',
            click(item, focusedWindow) {
                focusedWindow.webContents.openDevTools();
            },
        })
    }
    const menu = Menu.buildFromTemplate(templateMenu);
    Menu.setApplicationMenu(menu);
}
var splash;
var WhaleMemo = new WhaleMemo(app, () => {
    splash && splash.close()
});
app.on('ready', () => {
    if (initialised) {
        return;
    }
    splash = (() => {
        const screenSize = require("electron").screen.getPrimaryDisplay().workAreaSize;
        const splash = new BrowserWindow({
            title: "Whale Memo",
            x: (screenSize.width - 500) / 2,
            y: (screenSize.height - 300) / 2,
            width: 500,
            height: 300,
            frame: false,
            show: false,
        });
        splash.once('ready-to-show', () => { splash.show(); });
        splash.loadURL("file://" + __dirname + "/../html/splash.html")
        return {
            close() {
                splash.close();
            }
        }
    })();
});

