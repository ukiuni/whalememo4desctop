
function updateCSS() {
    var textarea = document.getElementById("content");
    var div = document.getElementById("marked");
    allFit(textarea, div);
};
function allFit(src, dist) {
    if (dist == null) {
        return;
    }
    fitCSS(src, dist);
    for (var i = 0; i <= dist.childElementCount; i++) {
        allFit(src, dist.children[i]);
    }
}
var fitCSSList = ['border-bottom-width', 'border-left-width', 'border-right-width',
    'border-top-width', 'font-family', 'font-size', 'font-style',
    'font-variant', 'font-weight', 'letter-spacing',
    'word-spacing', 'line-height', 'padding-bottom', 'padding-left',
    'padding-right', 'padding-top', 'text-decoration'];

function fitCSS(src, dist) {
    for (var i = 0; i < fitCSSList.length; i++) {
        dist.style[fitCSSList[i]] = src.style[fitCSSList[i]];
    }
}
setTimeout(updateCSS);