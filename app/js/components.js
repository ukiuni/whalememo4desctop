(async function init() {
    const { ipcRenderer } = require('electron');
    var memoHTML = (await readFile(__dirname + "/../html/note.html")).toString();
    var settingHTML = (await readFile(__dirname + "/../html/setting.html")).toString();
    var licenseHTML = (await readFile(__dirname + "/../html/license.html")).toString();
    var licenseTXT = (await readFile(__dirname + "/../html/license.txt")).toString();
    var modalHTML = (await readFile(__dirname + "/../html/modal.html")).toString();
    var footerHTML = (await readFile(__dirname + "/../html/footer.html")).toString();
    var toggleHTML = (await readFile(__dirname + "/../html/toggle.html")).toString();
    var settings = null;
    var memos = null;
    try {
        settings = JSON.parse(await readFile(SETTINGS_FILE));
    } catch (e) {
    }
    if (!settings || !settings.version) {
        settings = {
            version: 1,
            shareStorage: "File",
            path: MEMO_FILE,
            saveInterval: 10000,
            mail: "",
            name: "",
            repository: "master",
            gitUrl: null,
            gitToken: null,
            repository: null
        };
    }
    try {
        memos = JSON.parse(await readFile(MEMO_FILE));
    } catch (ignore) {
    }

    if (!memos) {
        memos = [];
    }

    if (settings.saveToServer) {
        await syncronizeWithServer(settings, memos, null, (e) => { console.log(e) });
    }

    if (memos.length == 0) {
        memos.push({
            content: "# Whale memo\nWhale memo is memo with markdown.",
            id: createRandomString(32),
            updatedAt: new Date().getTime()
        });
    }

    memos.forEach(m => m.deleted = !!m.deleted);

    Vue.use({
        install: function (Vue, options) {
            Vue.prototype.$messageEvent = new Vue()
        }
    });

    Vue.component('modal', {
        template: modalHTML,
        props: { title: String, body: String, footer: String, showInput: Boolean, inputValue: String }
    });

    Vue.component('license', {
        template: licenseHTML,
        data: function () {
            return {
                licenseText: licenseTXT
            }
        },
        methods: {
            close() {
                this.$emit('onclose')
            }
        }
    });

    Vue.component('setting', {
        template: settingHTML,
        data: function () {
            return {
                saveToServer: settings.saveToServer,
                url: settings.url || "https://api.whalememo.com",
                mail: settings.mail,
                accessKey: settings.accessKey,
            }
        },
        methods: {
            save: async function () {
                const saveAndClose = () => {
                    this.$messageEvent.$emit("saveSetting", {
                        saveToServer: this.saveToServer,
                        url: this.url,
                        mail: this.mail,
                        accessKey: this.accessKey
                    });
                    this.$emit('onclose')
                }
                if (this.saveToServer) {
                    var accessSetting = { url: this.url, accessKey: this.accessKey };
                    createHttpClient(accessSetting).post("/users/requestActivation", { mail: this.mail }).catch((e) => {
                        setTimeout(() => {
                            this.$messageEvent.$emit("hideModal");
                            this.$messageEvent.$emit("event", "Can't connect server");
                        }, 500);
                    });
                    this.$messageEvent.$emit("showModal", {
                        title: "Account setting", body: "Code sended to " + this.mail + " .\nInput code.", footer: "", showInput: true, exec: async (code) => {
                            try {
                                this.accessKey = (await createHttpClient(accessSetting).post(this.url + "/users/activate", { mail: this.mail, code: code, hostname: require("os").hostname() })).data.accessKey;
                                saveAndClose();
                                setTimeout(() => this.$messageEvent.$emit("requestSynce"), 1000);
                            } catch (e) {
                                this.saveToServer = false;
                                this.$messageEvent.$emit("event", "Can't activate account: " + e);
                            }
                        }
                    });
                } else {
                    saveAndClose();
                }
            }
        }
    });
    Vue.component("app-footer", {
        props: { message: String, showProgress: Boolean },
        template: footerHTML
    });
    var timeoutID;
    var draggingNote = null;
    var initialized = false;
    Vue.component("note", {
        data: () => {
            return {
                memos: memos,
                searchText: "",
                currentMemo: memos.filter(m => !m.deleted)[0],
                isCurrentMemoEdited: false,
                saving: false,
                showMarkdown: getShowMarkdown(),
                toastOptions: {
                    usageStatistics: false,
                    hideModeSwitch: true,
                    useDefaultHTMLSanitizer: false,
                    useCommandShortcut: false,
                    linkAttribute: {
                        target: '_blank',
                        contenteditable: 'false',
                        rel: 'noopener noreferrer'
                    }
                },
                oneOpenNotes: [],
                settings,
                syncing: false,
                veryAfterChanged: true,
                nowOpenSubWindow: false
            }
        },
        computed: {
            searchedMemos: function () {
                if (!this.searchText) {
                    return this.memos.filter(note => !note.deleted);
                }
                return this.memos.filter(note => !note.deleted && note.content.includes(this.searchText))
            },
            markdownMode: function () {
                return this.showMarkdown ? "vertical" : "tab";
            }
        },
        template: memoHTML,
        methods: {
            dragStart: function (note) {
                draggingNote = note;
                draggingTitle = marked(note.content.split("\n", 1)[0]).replace(/<.+?>/g, "");
            },
            toMarkdown: function (text) {
                if (!text) {
                    return text;
                }
                var linebreakReplaced = text.replace(/(\n)|(\r\n)/g, "<br/>\n").replace(/\|<br\/>\n\|/g, "|\n").replace(/\|<br\/>\n([^\|])/g, "|\n\n$1").replace("\n\|", "\n\n|");
                setTimeout(updateCSS);
                var markedown = marked(linebreakReplaced, { breaks: false });
                return markedown;
            },
            update: function (note) {
                if (this.veryAfterChanged) {
                    this.veryAfterChanged = false;
                    return;
                }
                this.isCurrentMemoEdited = true;
                note.updatedAt = new Date().getTime();
                if (this.memos[0].id != note.id) {
                    this.memos.forEach((m, index) => {
                        if (m.id == note.id) {
                            this.memos.splice(index, 1);
                        }
                    });
                    this.memos.unshift(note);
                }
                if (this.oneOpenNotes[note.id]) {
                    ipcRenderer.send("updateNote", note);
                }
                this.startSaveTimer(note);
            },
            getTitle: function (note) {
                if (!note || !note.content) {
                    return;
                }
                return marked(note.content.split("\n", 1)[0]).replace(/<.+?>/g, "");
            },
            getContent: function (note, length) {
                if (!note || !note.content) {
                    return;
                }
                var contents = note.content.split("\n", 10);
                contents.shift()
                var content = contents.find((c) => c);
                if (!content) {
                    return;
                }
                content.replace(/\n+/, "");
                return marked(content).replace(/<.+?>/g, "").substring(0, length);
            },
            change: function (memo) {
                this.veryAfterChanged = true;
                if (this.isCurrentMemoEdited) {
                    this.save(this.currentMemo);
                }
                this.currentMemo = memo;
                this.isCurrentMemoEdited = false;
                setTimeout(() => {
                    this.$refs.editor.invoke('focus');
                })
            },
            startSaveTimer: function (note) {
                if (timeoutID) {
                    clearTimeout(timeoutID);
                    timeoutID = null;
                }
                timeoutID = setTimeout(() => { this.save(note) }, settings.saveInterval || 10000);
            },
            newNote: function (newContent) {
                if (this.isCurrentMemoEdited) {
                    this.save(this.currentMemo);
                }
                this.isCurrentMemoEdited = false;
                var newMemo = this.currentMemo = {
                    content: newContent || "New Memo",
                    deleted: false,
                    id: createRandomString(32),
                    updatedAt: new Date().getTime()
                }
                this.memos.unshift(newMemo);
                setTimeout(() => {
                    this.$refs.editor.invoke('focus');
                })
                this.$messageEvent.$emit("event", "New note created.");
            },
            save: async function (note) {
                if (!note) {
                    note = this.currentMemo;
                }
                if (this.saving) {
                    return;
                }
                this.saving = true;
                this.$messageEvent.$emit("event", { message: "saving", showProgress: true });
                this.isCurrentMemoEdited = false;
                if (timeoutID) {
                    clearTimeout(timeoutID);
                    timeoutID = null;
                }
                await writeFile(MEMO_FILE, JSON.stringify(this.memos));
                this.$messageEvent.$emit("event", "saved");
                this.saving = false;
            },
            delete: async function (note) {
                note.deleted = true;
                note.updatedAt = new Date().getTime();
                await this.save();
                this.$messageEvent.$emit("event", "memo was deleted");
            },
            openContext: function (event, note) {
                event.preventDefault();
                var contentMenu = document.getElementById("contextMenu" + note.id);
                contentMenu.style.display = "inline";
                contentMenu.style.top = (event.clientY - 10) + "px";
                contentMenu.style.left = (event.clientX - 10) + "px";
            },
            hideContentMenu: function (note) {
                var contentMenu = document.getElementById("contextMenu" + note.id);
                contentMenu.style.display = "none";
            },
            showDeleteModal: function (note) {
                this.hideContentMenu(note);
                this.$messageEvent.$emit("showModal", {
                    title: "Delete", body: "Delete this memo?", footer: "", exec: () => {
                        this.delete(note);
                    }
                });
            },
            markAsResolveConflict: function (note) {
                this.hideContentMenu(note);
                note.conflictedToId = null;
                note.updatedAt = new Date().getTime();
                this.save(note);
            },
            openNewWindow: function (note) {
                this.nowOpenSubWindow = true;
                this.oneOpenNotes[note.id] = note;
                ipcRenderer.send("openNewWindow", note);
            },
            synce: function () {
                this.syncing = true;
                syncronizeWithServer(settings, memos, () => {
                    this.$messageEvent.$emit("event", "Synced with server");
                    this.syncing = false;
                }, (e) => {
                    this.$messageEvent.$emit("event", "Sync with server is failed: " + e);
                    this.syncing = false;
                });
            }
        },
        mounted: function () {
            this.$messageEvent.$on("requestSynce", () => {
                this.synce();
            });
            ipcRenderer.on('onBlur', async (msg) => {
                if (this.nowOpenSubWindow) {
                    this.nowOpenSubWindow = false;
                    return;
                }
                if (this.syncing) {
                    return;
                }
                this.syncing = true;
                syncronizeWithServer(settings, memos, () => this.syncing = false, () => this.syncing = false);
            });
            ipcRenderer.on('onWindowClosing', async (msg) => {
                var finishWindow = () => {
                    ipcRenderer.send("closeCommandAfterSynce");
                };
                if (this.syncing) {
                    return;
                }
                this.syncing = true;
                await this.save();
                if (settings.saveToServer) {
                    syncronizeWithServer(settings, memos, finishWindow, finishWindow);
                } else {
                    finishWindow();
                }
            });
            this.$messageEvent.$on("requestSave", () => {
                this.save(this.currentMemo);
            });

            this.$messageEvent.$on("openNote", (shortcut) => {
                this.change(memos.find(m => m.id == shortcut.id));
            });
            this.$messageEvent.$on("requestNewNote", (content) => {
                this.newNote(content);
            });
            const ipcMain = require("electron").remote.ipcMain;
            ipcMain.on("oneWindowNoteUpdated", (event, note) => {
                var noteOfThisComponent = this.oneOpenNotes[note.id];
                noteOfThisComponent.content = note.content;
                //this.update(noteOfThisComponent);
            });
            ipcRenderer.on("windowClosed", (event, note) => {
                delete this.oneOpenNotes[note.id];
            })
            this.$messageEvent.$on("showMarkdown", (showMarkdown) => {
                this.showMarkdown = showMarkdown;
                setShowMarkdown(showMarkdown);
            });
            if (!initialized) {
                ipcRenderer.send("initialized");
            }
        },
        watch: {
            showMarkdown: function (showMarkdown) {
                setShowMarkdown(showMarkdown);
            },
            searchText: function (searchText) {
                document.getElementsByClassName("ukiuni-vertual-scroll")[0].scrollTop = "0px";
            }
        }
    });

    Vue.component("virtual-scroll", require("../js/virtual-scroll"));

    var toastui = require('@toast-ui/vue-editor');
    var Editor = toastui.Editor;
    Vue.component("markdown-editor", Editor);

    var ToggleComponent = Vue.extend({
        template: toggleHTML,
        data: function () {
            return {
                showMarkdown: getShowMarkdown()
            }
        },
        watch: {
            showMarkdown: function (showMarkdown) {
                this.$messageEvent.$emit("showMarkdown", showMarkdown)
            }
        }
    })
    var importEvetnoteFile = async (filePath, $messageEvent) => {
        var toMd5 = (arg) => {
            var crypto = require("crypto");
            var md5 = crypto.createHash('md5');
            md5.update(Buffer.from(arg.replace(/^.*,/, ''), 'base64'));
            return md5.digest('hex');
        };
        var sax = require("../vender/sax"),
            strict = true, // set to false for html-mode
            parser = sax.createStream(strict);
        let fs = require('fs');
        var inTitle = false;
        var inData = false;
        var title = null;
        var cdata = null;
        var TurndownService = require('turndown');
        var turndownService = new TurndownService()
        var images;
        var importContents = [];
        parser.on('opentag', (node) => {
            inTitle = node.name == "title";
            inData = node.name == "data";
            if (node.name == "note") {
                images = [];
            }
        });
        parser.on('closetag', name => {
            if (name == "note") {
                if (cdata.indexOf("standalone=\"yes\"") > 0) {
                    return;
                }
                var markdown;
                try {
                    var rep = cdata.replace(/<en-media hash="([^"]*)".*?type="([^"]*)".*?\/>/g, (arg, hash, type) => {
                        return `<img src="data:${type};base64,${images[hash]}"/>`
                    });
                    markdown = turndownService.turndown(rep);//.replace(/<[^>]+>/gi, "");
                } catch (e) {
                    markdown = cdata;
                }
                var content = "# " + (title || "no title") + "\n" + markdown
                importContents.push(content);

                $messageEvent.$emit("event", "importing... " + importContents.length);
            }
        });
        parser.on('text', text => {
            if (inTitle) {
                title = text;
            }
            if (inData) {
                var linedBase64 = text.replace(/[\n|\r]/g, "");
                images[toMd5(linedBase64)] = linedBase64;
            }
        });
        parser.on('error', err => {
            console.log("error " + err);
        });
        parser.on('end', async () => {
            await writeFile(MEMO_FILE, JSON.stringify(memos));
            importContents.reverse().forEach((content) => $messageEvent.$emit("requestNewNote", content))
            $messageEvent.$emit("event", "imported");
        });
        parser.on('cdata', _cdata => {
            cdata = _cdata;
        });
        let stream = fs.createReadStream(filePath + "");
        stream.pipe(parser);
    }
    var vue = new Vue({
        el: '#app',
        data: {
            showModal: false,
            modalMessage: {
                title: "",
                body: "",
                footer: "",
                exec: "",
                showInput: true,
                inputValue: ""
            },
            footer: {
                message: "",
                showProgress: false
            },
            showDisplay: 1,
            settings,
            shortcuts: [],
            draggingShortcut: null
        },
        methods: {
            dropped: async function () {
                if (draggingNote && !this.shortcuts.some(n => n.id == draggingNote.id)) {
                    this.shortcuts.push(draggingNote);
                    await writeFile(SHORTCUTS_FILE, JSON.stringify(this.shortcuts));
                    draggingNote = null;
                } else if (this.draggingShortcut) {
                    this.draggingShortcut = null;
                }
            },
            toIconContent: function (content) {
                return marked(content).replace(/<.+?>/g, "").replace("\n", " ");
            },
            shortcutSelected: function (shortcut) {
                this.$messageEvent.$emit("openNote", shortcut);
            },
            openShortcutContext: function (event, note) {
                event.preventDefault();
                var contentMenu = document.getElementById("shortcutContextMenu" + note.id);
                contentMenu.style.display = "inline";
                contentMenu.style.top = (event.clientY - 10) + "px";
                contentMenu.style.left = (event.clientX - 10) + "px";
            },
            removeShortcut: async function (note) {
                this.shortcuts = this.shortcuts.filter(n => n.id != note.id);
                await writeFile(SHORTCUTS_FILE, JSON.stringify(this.shortcuts));
            },
            hideShortcutContextMenu: function (note) {
                var contentMenu = document.getElementById("shortcutContextMenu" + note.id);
                contentMenu.style.display = "none";
            },
            shortcutDragStart: function (shortcut) {
                this.draggingShortcut = shortcut;
            },
            shrotcutDropped: function (shortcut) {
                if (!this.draggingShortcut) {
                    return;
                }
                this.removeShortcut(this.draggingShortcut);
                this.shortcuts.splice(this.shortcuts ? this.shortcuts.findIndex(s => s.id == shortcut.id) + 1 : 0, 0, this.draggingShortcut);
                Array.prototype.forEach.call(document.querySelectorAll(".shortcut"), e => e.style.opacity = 1.0);
                this.draggingShortcut = null;
            },
            appendsMarkdownToggle: function () {
                var toolBar = document.getElementsByClassName("tui-editor-defaultUI-toolbar")[0];
                var toggle = new ToggleComponent().$mount();
                toolBar.insertBefore(toggle.$el, toolBar.firstChild);
            }
        },
        mounted: async function () {
            try {
                this.shortcuts = JSON.parse(await readFile(SHORTCUTS_FILE)).map(s => memos.find(m => m.id == s.id));
            } catch (e) {
                // at first. no file.
            }
            this.appendsMarkdownToggle();
            ipcRenderer.on('save', (msg) => {
                if (this.showDisplay == 1) {
                    this.$messageEvent.$emit("requestSave");
                }
            });
            ipcRenderer.on('append', (msg) => {
                if (this.showDisplay == 1) {
                    this.$messageEvent.$emit("requestNewNote");
                }
            });
            ipcRenderer.on('importFile', (event, filePath) => {
                importEvetnoteFile(filePath, this.$messageEvent);
            });

            this.$messageEvent.$on('event', (message) => {
                if (!!message.showProgress) {
                    this.footer.message = message.message;
                    this.footer.showProgress = true;
                } else {
                    this.footer.message = message;
                    this.footer.showProgress = false;
                }
            });

            this.$messageEvent.$on('showModal', (message) => {
                this.showModal = false;
            });
            this.$messageEvent.$on('showModal', (message) => {
                this.showModal = true;
                this.modalMessage.title = message.title;
                this.modalMessage.body = message.body;
                this.modalMessage.footer = message.footer;
                this.modalMessage.showInput = message.showInput;
                this.modalMessage.inputValue = "";
                this.modalMessage.exec = (inputValue) => {
                    message.exec(inputValue);
                    this.showModal = false;
                }
            });
            this.$messageEvent.$on('saveSetting', async (settings) => {
                Object.assign(this.settings, settings);
                await writeFile(SETTINGS_FILE, JSON.stringify(this.settings));
                this.$messageEvent.$emit("event", "settings are saved");
            });
        },
        watch: {
            showDisplay: function (showDisplay) {
                if (showDisplay == 1) {
                    setTimeout(() => {
                        this.appendsMarkdownToggle();
                    });
                }
            }
        }
    });
})();