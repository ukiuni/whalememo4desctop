const fs = require('fs');
const homedir = require('os').homedir();
const WHALEMEMO_DIR = homedir + "/.whalememo"
try {
    fs.mkdirSync(WHALEMEMO_DIR);
} catch (ignore) {
}
const MEMO_FILE = WHALEMEMO_DIR + "/memo.json";
const SETTINGS_FILE = WHALEMEMO_DIR + "/settings.json";
const SHORTCUTS_FILE = WHALEMEMO_DIR + "/shortcuts.json";
const SYNCED_INFO_FILE = WHALEMEMO_DIR + "/synced.json";

function readFile(path) {
    return new Promise((resolve, reject) => {
        fs.readFile(path, (error, data) => {
            if (error != null) {
                reject(error);
                return;
            }
            resolve(data);
        })
    })
}
function writeFile(path, data) {
    return new Promise((resolve, reject) => {
        fs.writeFile(path, data, (error) => {
            if (error != null) {
                reject(error);
                return;
            }
            resolve(path);
        })
    });
}
const BASE62TABLE = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
function createRandomString(length) {
    var randomString = []
    var length62 = BASE62TABLE.length;
    for (var i = 0; i < length; i++) {
        randomString.push(BASE62TABLE[Math.floor(Math.random() * length62)]);
    }
    return randomString.join("");
}
const STORAGE_KEY_HIDE_MARKDOWN = "HIDE_markdown"
function getShowMarkdown() {
    return localStorage.getItem(STORAGE_KEY_HIDE_MARKDOWN) == "false";
}
function setShowMarkdown(bool) {
    localStorage.setItem(STORAGE_KEY_HIDE_MARKDOWN, !bool);
}
function createHttpClient(settings) {
    headers = {
        'Content-Type': 'application/json'
    }
    if (settings.accessKey) {
        headers['Authentication'] = settings.accessKey
    }
    return require("axios").create({
        baseURL: settings.url,
        headers,
        responseType: 'json'
    });
}