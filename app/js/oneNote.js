
var toastui = require('@toast-ui/vue-editor');
var Editor = toastui.Editor;
var toggleHTML;
(async () => {
    toggleHTML = (await readFile(readFile +"../html/toggle.html")).toString();
})();
Vue.use({
    install: function (Vue, options) {
        Vue.prototype.$messageEvent = new Vue()
    }
});

Vue.component("markdown-editor", Editor);
const note = require('electron').remote.getCurrentWindow().note;
const ipcRenderer = require('electron').ipcRenderer;
var vue = new Vue({
    el: '#app',
    data: function () {
        return {
            note,
            showMarkdown: false,
            toastOptions: {
                usageStatistics: false,
                hideModeSwitch: true,
                useDefaultHTMLSanitizer: false,
                useCommandShortcut: false,
                linkAttribute: {
                    target: '_blank',
                    contenteditable: 'false',
                    rel: 'noopener noreferrer'
                }
            }
        }
    },
    computed: {
        markdownMode: function () {
            return this.showMarkdown ? "vertical" : "tab";
        }
    },
    methods: {
        update: function (note) {
            ipcRenderer.send('oneWindowNoteUpdated', note);
        }
    },
    mounted: function () {
        var appendShowMarkdownToggle = () => {
            if (!toggleHTML) {
                setTimeout(appendShowMarkdownToggle);
                return;
            }
            var ToggleComponent = Vue.extend({
                template: toggleHTML,
                data: function () {
                    return {
                        showMarkdown: false
                    }
                },
                watch: {
                    showMarkdown: function (showMarkdown) {
                        this.$messageEvent.$emit("showMarkdown", showMarkdown)
                    }
                }
            });
            var toolBar = document.getElementsByClassName("tui-editor-defaultUI-toolbar")[0];
            var toggle = new ToggleComponent().$mount();
            toolBar.insertBefore(toggle.$el, toolBar.firstChild);
        }
        setTimeout(appendShowMarkdownToggle);
        this.$messageEvent.$on("showMarkdown", (showMarkdown) => {
            this.showMarkdown = showMarkdown;
        });
        ipcRenderer.on('updateNote', (event, note) => {
            if (note.id == this.note.id) {
                this.note.content = note.content;
                this.$refs.editor.invoke('setValue', this.note.content);
            }
        })
    }
});
