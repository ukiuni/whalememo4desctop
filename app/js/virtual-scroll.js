const template = `
<div class="ukiuni-vertual-scroll">
  <div ref="virtualScrollInner">
    <div ref="displayListWrapper">
      <div v-for="item in inListItem">
        <slot :memo="item"></slot>
      </div>
    </div>
  </div>
</div>`;
const ORIENTATION_VERTICAL = "vertical";
module.exports = {
    template,
    props: {
        items: Array,
        itemSize: Number,
        orientation: {
            type: String,
            default: ORIENTATION_VERTICAL
        }
    },
    data: function () {
        return {
            scrolling: 0,
            viewSize: 0,
            scrollOrResizeParameters: null
        }
    },
    computed: {
        inListItem: function () {
            return this.items.slice(Math.floor(this.scrolling / this.itemSize),
                Math.floor((this.scrolling + this.viewSize) / this.itemSize) + 1);
        }
    },
    methods: {
        updateText: function (e) {
            this.$emit('input', e.target.value)
        },
        scrollOrResize: function ({ element, inner, wrapper, scrollCounter, viewSizeTarget, calcSizeSide, scrollSide }) {
            this.scrolling = element[scrollCounter];
            this.viewSize = element[viewSizeTarget];
            var innerSize = (this.items.length * this.itemSize);
            var displayListSize = (Math.floor(this.viewSize / this.itemSize) + 1) * this.itemSize;
            var scrollSize = (this.scrolling - this.scrolling % this.itemSize);
            if (scrollSize > innerSize - displayListSize) {
                scrollSize = innerSize - displayListSize;
            }
            if (scrollSize < 0) {
                scrollSize = 0;
            }
            inner.style[calcSizeSide] = innerSize + "px";
            wrapper.style[calcSizeSide] = displayListSize + "px";
            wrapper.style[scrollSide] = scrollSize + "px";
        }
    },
    mounted: function () {
        var isVertical = this.orientation == ORIENTATION_VERTICAL;
        var element = this.$el;
        element.style[isVertical ? "overflow-y" : "overflow-x"] = "scroll";
        element.style[isVertical ? "height" : "width"] = "100%";
        element.style.boxSizing = "border-box";
        var inner = this.$refs.virtualScrollInner;
        inner.style.position = "relative"
        var wrapper = this.$refs.displayListWrapper;
        wrapper.style.position = "absolute"
        wrapper.style[isVertical ? "width" : "height"] = "100%"
        wrapper.style.boxSizing = "border-box";

        var scrollCounter = isVertical ? "scrollTop" : "scrollLeft";
        var viewSizeTarget = isVertical ? "offsetHeight" : "offsetWidth";
        var calcSizeSide = isVertical ? "height" : "width";
        var scrollSide = isVertical ? "top" : "left";
        var scrollOrResize = () => {
            this.scrollOrResize({ element, inner, wrapper, scrollCounter, viewSizeTarget, calcSizeSide, scrollSide });
        }
        window.onresize = scrollOrResize;
        element.onscroll = scrollOrResize;
        inner.onscroll = scrollOrResize;
        element.addEventListener("scroll", scrollOrResize);
        inner.addEventListener("scroll", scrollOrResize);
        scrollOrResize();
    },
    watch: {
        items: function () {
            if (this.scrollOrResizeParameters) {
                this.scrollOrResize(this.scrollOrResizeParameters)
            }
        }
    }
};
