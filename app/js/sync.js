async function loadSyncedInfo(settings) {
    var syncedInfo;
    try {
        syncedInfo = JSON.parse(await readFile(SYNCED_INFO_FILE));
    } catch (ignore) {
    }
    if (!syncedInfo) {
        syncedInfo = {
            url: settings.url,
            lastSynced: 0
        }
    }
    return syncedInfo;
}
async function syncronizeWithServer(settings, memos, onSuccess, onError) {
    var syncedInfo = await loadSyncedInfo(settings);
    var syncedAt = new Date().getTime();
    var lastSynced = settings.url == syncedInfo.url ? syncedInfo.lastSynced || 0 : 0;

    try {
        var loadedMemos = (await createHttpClient(settings).get("/notes?lastUpdatedAt=" + lastSynced)).data;
        if (!loadedMemos) {
            return;
        }
        var localMemoDictionaly = [];
        memos.forEach(m => localMemoDictionaly[m.id] = m);

        var conflictedLocalMemos = [];
        loadedMemos.forEach(loadedMemo => {
            var localMemo = localMemoDictionaly[loadedMemo.id];
            if (!localMemo) {
                memos.push(loadedMemo);
            } else if (localMemo.syncedUpdatedAt == localMemo.updatedAt) {
                Object.assign(localMemo, loadedMemo);
            } else if (loadedMemo.content == localMemo.content) {//TODO in case remote updated. keep local change not implemented
                Object.assign(localMemo, loadedMemo);
                //prior to deleted
                localMemo.deleted = localMemo.deleted || loadedMemo.deleted;
                //prior to resolve conflicted;
                localMemo.conflictedToId = (localMemo.conflictedToId == null || localMemo.conflictedToId == null) ? null : localMemo.conflictedToId || loadedMemo.conflictedToId;
            } else {//else case is if (localMemo.syncedUpdatedAt != loadedMemo.syncedUpdatedAt) 
                //conflict
                conflictedLocalMemos.push(localMemo);
            }
        });
        conflictedLocalMemos.forEach(m => {
            m.syncedUpdatedAt = 0;
            m.id = createRandomString(32);
            memos.push(m);
        });
    } catch (e) {
        if (onError) {
            onError(e);
            return;
        } else {
            throw e;
        }
    }
    try {
        await uploadNotSynced({ settings, memos });
    } catch (e) {
        if (onError) {
            onError();
            return;
        } else {
            throw e;
        }
    }

    memos.sort((a, b) => b.updatedAt - a.updatedAt);

    syncedInfo.url = settings.url;
    syncedInfo.lastSynced = syncedAt;
    await writeFile(SYNCED_INFO_FILE, JSON.stringify(syncedInfo));
    await writeFile(MEMO_FILE, JSON.stringify(memos));
    if (onSuccess) {
        onSuccess();
    }
}

async function uploadNotSynced({ settings, memos, onSuccess, onError }) {
    var synceMemos = memos.filter((m) => m.syncedUpdatedAt != m.updatedAt);

    var syncedError = null;
    if (synceMemos.length > 0) {
        await uploadAndSyncedMemos({
            settings, memos, registMemos: synceMemos,
            onError(e) {
                syncedError = e || "sync error";
            }
        });
        return;
    }
    if (syncedError) {
        if (onError) {
            onError(syncedError);
            return;
        } else {
            throw syncedError
        }
    }
    if (onSuccess) {
        onSuccess();
    }
}
async function uploadAndSyncedMemos({ settings, memos, registMemos, onSaved, onConflict, onError, onFinally }) {
    try {
        registMemos = registMemos.filter(n => n.syncedUpdatedAt != n.updatedAt);
        if (registMemos.length == 0) {
            onError("no data to be regist")
            onFinally();
            return;
        }
        var response = await createHttpClient(settings).post(settings.url + "/notes", registMemos);
        var responsedMemos = response.data;
        var registedMemos = [];
        var conflictedMemos = [];
        responsedMemos.forEach(m => registMemos.find(r => m.id == r.id) ? registedMemos.push(m) : conflictedMemos.push(m));
        registedMemos.forEach((registedMemo) => {
            var registMemo = memos.find(m => m.id == registedMemo.id);
            //start oncase localUpdated
            var content = registMemo.content;
            var updatedAt = registMemo.updatedAt;
            //end oncase localUpdated
            Object.assign(registMemo, registedMemo);
            registMemo.content = content;
            registMemo.updatedAt = updatedAt;
            registMemo.syncedAt = new Date().getTime();
            if (onSaved) {
                onSaved(registedMemo);
            }
        });
        conflictedMemos.forEach((registedMemo) => {
            registedMemo.syncedAt = new Date().getTime();
            memos.unshift(registedMemo)
            //何やってるかわからない。Conflictしたので、なんでもとのやつを上書きする？
            // var conflictedResponsedNote = responsedMemos.find(m => m.id == registedMemo.conflictedToId);
            // var conflictSrcMemo = memos.find(m => m.id == conflictedResponsedNote.id);
            // Object.assign(conflictSrcMemo, conflictedResponsedNote);
            if (onConflict) {
                onConflict(registedMemo);
            }
        });
    } catch (e) {
        if (onError) {
            onError(e);
        }
        throw e;
    } finally {
        if (onFinally) {
            onFinally();
        }
    }
}